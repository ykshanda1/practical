<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::group(['middleware'=>'auth'], function(){
	Route::group(['prefix'=>'customers'], function(){

		Route::get('/', 'CustomerController@index')->name('customers.index');
		Route::post('/', 'CustomerController@store')->name('customers.store');
		Route::get('/create', 'CustomerController@create')->name('customers.create');
		Route::get('/show/{id}', 'CustomerController@show')->name('customers.show');
		Route::get('/edit/{id}', 'CustomerController@edit')->name('customers.edit');
		Route::put('/update/{id}', 'CustomerController@update')->name('customers.update');
		Route::delete('/destroy/{id}', 'CustomerController@destroy')->name('customers.destroy');

	});

	Route::group(['prefix'=>'invoices'], function(){

		Route::get('/', 'InvoiceController@index')->name('invoices.index');
		Route::post('/', 'InvoiceController@store')->name('invoices.store');
		Route::get('/create', 'InvoiceController@create')->name('invoices.create');
		Route::get('/show/{id}', 'InvoiceController@show')->name('invoices.show');
		Route::get('/edit/{id}', 'InvoiceController@edit')->name('invoices.edit');
		Route::put('/update/{id}', 'InvoiceController@update')->name('invoices.update');
		Route::delete('/destroy/{id}', 'InvoiceController@destroy')->name('invoices.destroy');

	});

});