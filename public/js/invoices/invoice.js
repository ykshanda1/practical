var Invoice = function () {

    var table = $('#invoicesTable');
    var dataTable = null;
    var initDataTable = function(){
        dataTable = table.DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            order:[],
            pageLength:25
            
        });
    }

    var deleteInvoice = function () {
        $('.delete-btn').click(function(event){
            event.preventDefault();
            let button = $(this);
            var token = $("meta[name='csrf-token']").attr("content");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url:button.attr('href'),
                        type:'delete',
                        data: {
                            "_token": token,
                        },
                        dataType: 'json',
                        beforeSend:function(){
                            button.attr('disabled',true);
                        },
                        success:function(response){
                            if (response.status === 'success') {
                                Swal.fire(
                                    'Deleted!',
                                    'Invoice has been deleted.',
                                    'success'
                                );
                                dataTable.row(button.parents('tr')).remove().draw();
                            }else{
                                $message = "Failed to delete Invoice.";
                                if(response.message !== undefined && response.message !== null){
                                    message = response.message;
                                }
                                Swal.fire('Failed', $message,'error');
                            }
                        },
                        error:function(){
                            Swal.fire('Failed', 'Failed to delete Invoice.','error');
                        },
                        complete:function(){
                            button.attr('disabled',false);
                        }
                    });
                }
            });
        });
    };

    var validateForm = function () {
        $('#manageInvoice').validate({
            ignore : ':hidden',
            rules: {
                customer_id: {
                    required: true,
                },
                invoice_date: {
                    required: true,
                    date: true,
                },
                due_date: {
                    required: true,
                    date: true,
                },
                no_of_items:{
                    required: true,
                    integer: true,
                },
                amount:{
                    required: true,
                    integer: true,
                },
            },

            messages: {
                customer_id: {
                    required: "Please select Customer name",
                },
                invoice_date: {
                    required: "Please enter invoice date",
                    date: "Please enter date only",
                },
                due_date: {
                    required: "Please enter due date",
                    date: "Please enter date only",
                },
                amount:{
                    required: "Please enter amount",
                    integer: "Please enter numeric value only",
                },
            },

            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                error.addClass('text-danger');
                element.closest('.col-md-10').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    };

    

    return {
        deleteInvoice: function () {
            deleteInvoice();
        },
        list: function () {
            initDataTable();
        },
        validateForm: function () {
            validateForm();
        },
        
    }

}();
