var Customer = function () {

    var table = $('#customersTable');
    var dataTable = null;
    var initDataTable = function(){
        dataTable = table.DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            order:[],
            pageLength:25
            
        });
    }

    var deleteCustomer = function () {
        $('.delete-btn').click(function(event){
            event.preventDefault();
            let button = $(this);
            var token = $("meta[name='csrf-token']").attr("content");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url:button.attr('href'),
                        type:'delete',
                        data: {
                            "_token": token,
                        },
                        dataType: 'json',
                        beforeSend:function(){
                            button.attr('disabled',true);
                        },
                        success:function(response){
                            if (response.status === 'success') {
                                Swal.fire(
                                    'Deleted!',
                                    'Customer has been deleted.',
                                    'success'
                                );
                                dataTable.row(button.parents('tr')).remove().draw();
                            }else{
                                $message = "Failed to delete Customer.";
                                if(response.message !== undefined && response.message !== null){
                                    message = response.message;
                                }
                                Swal.fire('Failed', $message,'error');
                            }
                        },
                        error:function(){
                            Swal.fire('Failed', 'Failed to delete Customer.','error');
                        },
                        complete:function(){
                            button.attr('disabled',false);
                        }
                    });
                }
            });
        });
    };

    var validateForm = function () {
        $('#manageCustomer').validate({
            ignore : ':hidden',
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
            },

            messages: {
                name: {
                    required: "Please enter name",
                },
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email",
                },
            },

            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                error.addClass('text-danger');
                element.closest('.col-md-10').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    };

    

    return {
        deleteCustomer: function () {
            deleteCustomer();
        },
        list: function () {
            initDataTable();
        },
        validateForm: function () {
            validateForm();
        },
        
    }

}();
