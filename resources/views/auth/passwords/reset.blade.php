@extends('layouts.non-auth.app')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="{{ route('home')}}"><b>{{ config('app.name', 'Practical') }}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You are only one step a way from your new password, recover your password now.</p>

      <form action="{{ route('password.update') }}" id="resetPassword" method="post">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        
        <div class="input-group mb-3">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter your password" name="password" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input id="password-confirm"  placeholder="Enter you confirm-password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password_confirmation')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        {{ csrf_field() }}

        <div class="row">
          <div class="col-12">
            <button type="submit" id="resetPasswordBtn" class="btn btn-primary btn-block">Change password</button>
          </div>
          <!-- /.col -->
        </div>
        @if(Session::has('message'))
            <p style="color:red">{{ Session::get('message') }}</p>
        @endif
      </form>

      <p class="mt-3 mb-1">
        <a href="{{route('login')}}">Login</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection


@section('javascript')
<script src="{{ asset('public/js/admin/auth.js')}}"></script>
<script>
    $(document).ready(function(){
        Auth.resetPassword()    
        
    });
</script>
@endsection