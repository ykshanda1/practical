@extends('layouts.non-auth.app')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <a href="{{ route('home')}}"><b>{{ config('app.name', 'Practical') }}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>

      @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif

      <form action="{{ route('password.email') }}" id="forgotPassword" method="post">
        <div class="input-group mb-3">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Enter your email" required autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        {{ csrf_field() }}

        <div class="row">
          <div class="col-12">
            <button type="submit" id="forgotPasswordBtn" class="btn btn-primary btn-block">Request new password</button>
          </div>
          <!-- /.col -->
        </div>

        @if(Session::has('message'))
            <p style="color:red">{{ Session::get('message') }}</p>
        @endif
      </form>

      <p class="mt-3 mb-1">
        <a href="{{ route('login') }}">Login</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection


@section('javascript')
<script src="{{ asset('public/js/admin/auth.js')}}"></script>
<script>
    $(document).ready(function(){
        Auth.forgotPassword()    
        
    });
</script>
@endsection