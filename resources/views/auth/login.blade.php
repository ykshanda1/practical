@extends('layouts.non-auth.app')

@section('content')

<div class="login-box">
  <div class="login-logo">
    <a href="{{ route('home')}}"><b>{{ config('app.name', 'Practical') }}</b></a>
  </div>
  
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      
      <form action="{{ route('login') }}" id="login" method="POST">
        @csrf
        <div class="input-group mb-3">
          <input type="email" name="email" id="email"  class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}"  autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div> 
        <div class="input-group mb-3">
          <input type="password" name="password" id="password"  class="form-control @error('password') is-invalid @enderror" placeholder="Password"  autocomplete="current-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
        @if(Session::has('message'))
            <p style="color:red">{{ Session::get('message') }}</p>
        @endif
      </form>
      <p class="mb-1">
        @if (Route::has('password.request'))
            <a class="text-center" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection


@section('javascript')
<script src="{{ asset('public/js/admin/auth.js')}}"></script>
<script>
    $(document).ready(function(){
        Auth.login()
    });
</script>
@endsection