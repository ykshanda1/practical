@extends('layouts.auth.app')
@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Single Customer</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('customers.index')}}">Customer</a></li>
            <li class="breadcrumb-item active">Single Customer</li>
          </ol>
        </div>
      </div>
    </div>
  </section>


  <section class="content"> 
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Customer</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong>Name</strong>
              <p class="text-muted">
                {{ $customer->name }}
              </p>
              <hr>

              <strong>Email</strong>
              <p class="text-muted">
                {{ $customer->email }}
              </p>

              <br>
              <div class="row">
                <div class="col-md-offset-2  col-md-10">
                  <a href="{{ route('customers.index') }}" class="btn btn-danger">Back</a>
                </div>
              </div>
            </div>
              <!-- /.card-body -->
          </div>
        </div>
      </div> 
    </div>
  </section>

@endsection 

