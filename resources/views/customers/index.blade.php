@extends('layouts.auth.app')
@section('content')
<!-- Site wrapper -->
<div class="wrapper">
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Customers</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('customers.index')}}">Customers</a></li>
              <li class="breadcrumb-item active">Index</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('layouts.partials.alert-message')
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Customer Table </h3>
          <div class="card-tools">
            <a href="{{route('customers.create')}}">
                <button class="btn btn-success text-white">
                    <i class="fas fa-plus"></i>&nbsp;Add New
                </button>
            </a>  
          </div>
        </div>
        <div class="card-body">
            <table id="customersTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width: 12%" class="text-center">Name</th>
                        <th style="width: 10%" class="text-center">Email</th>
                        <th style="width: 12%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @if(!empty($customers))
                    @foreach($customers as $customer)
                    
                      <tr>
                          <td>{{$customer->name}}</td>
                          <td>{{$customer->email}}</td>
                          <td class="project-actions text-center">
                            <a class="btn btn-primary btn-close btn-xs" href="{{ route('customers.show',[$customer->id])}}">
                                <i class="fas fa-eye"></i>
                                View
                            </a>
                        
                            <a class="btn btn-info btn-close btn-xs" href="{{ route('customers.edit',[$customer->id])}}">
                                <i class="fas fa-pencil-alt"></i>
                                Edit
                            </a>
 
                            <a href="{{ route('customers.destroy',[$customer->id])}}"  class="btn btn-danger btn-close btn-xs delete-btn" style="color: white;" type="submit" >
                                <i class="fas fa-trash"></i>
                                Delete
                            </a>
                          </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
            </table>
          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
</div>  
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
  <script src="{{ asset("public/plugins/sweetalert2/sweetalert2.min.js")}}"></script>
  <script src="{{ asset("public/js/customer/customer.js") }}"></script>
  <script>
    $(document).ready(function(){
        Customer.deleteCustomer();
        Customer.list();
    });
    RedirectTo();
  </script>
@endsection
