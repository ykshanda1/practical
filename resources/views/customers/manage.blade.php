@extends('layouts.auth.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Customer</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('customers.index')}}">Customers</a></li>
              <li class="breadcrumb-item active">{{empty($customer)?'Add':'Edit'}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @include('layouts.partials.alert-message')
      <div class="row">
        <div class="col-md-12">
          <div class="card {{ empty($customer) ? 'card-success' : 'card-primary'}}">
            <div class="card-header">
              <h3 class="card-title">Customer {{empty($customer)?'Add':'Edit'}}</h3>
            </div>
            <div class="card-body">
            	<form action="{{ empty($customer) ? route('customers.store') : route('customers.update',[$customer->id]) }}"
               method="post" id="manageCustomer">
                  @csrf

                  @empty($customer)
                    @method('POST')
                  @else
                    @method('PUT')  
                  @endif  

                  <div class="form-group">
  	                <label for="name" class="col-md-2 control-label">Name</label>
  	                <div class="col-md-10">
  	                  <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Enter customer name" value="{{ !empty($customer)?$customer->name:'' }}">
                      @error('name')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>  
                      @enderror
  	                </div>
  	              </div>
                  <div class="form-group">
                    <label for="email" class="col-md-2 control-label">Email</label>
                    <div class="col-md-10">
                      <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Enter customer email" value="{{ !empty($customer)?$customer->email:'' }}">
                      @error('email')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>  
                      @enderror
                    </div>
                  </div>
  	              <div class="form-group">
  	                <div class="col-md-offset-2 col-md-10">
  	                  <button type="submit" class="btn btn-primary">Save</button>
  	                  <a href="{{ route('customers.index') }}" class="btn btn-danger">Cancel</a>
  	                </div>
  	              </div>
	            </form>
	         </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  <!-- /.content-wrapper -->
@endsection


@section('javascript')
  <script src="{{ asset("public/plugins/jquery-validation/jquery.validate.min.js") }}"></script>
  <script src="{{ asset("public/plugins/jquery-validation/additional-methods.min.js")}}"></script>
  <script src="{{ asset("public/js/customer/customer.js") }}"></script>
  <script>
    $(document).ready(function(){
        Customer.validateForm()
    });

    RedirectTo();

  </script>

@endsection
