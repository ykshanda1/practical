<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', "Practical") }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset("public/plugins/fontawesome-free/css/all.min.css") }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset("public/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css") }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset("public/plugins/icheck-bootstrap/icheck-bootstrap.min.css")}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("public/css/adminlte.min.css")}}">
  <link rel="stylesheet" href="{{ asset("public/css/adminlte.css")}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset("public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css")}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset("public/plugins/daterangepicker/daterangepicker.css")}}">
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
  <link rel="stylesheet" href="{{ asset("public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css") }}">
  <link rel="stylesheet" href="{{ asset("public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css") }}">
  <link rel="stylesheet" href="{{ asset("public/plugins/sweetalert2/sweetalert2.min.css") }}">
  <link rel="stylesheet" href="{{ asset("public/plugins/toastr/toastr.min.css") }}">

  <link rel="stylesheet" href="{{ asset("public/css/adminlte.min.css") }}">
  <link rel="stylesheet" href="{{ asset("public/plugins/select2/css/select2.min.css") }}">
  <link rel="stylesheet" href="{{ asset("public/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css") }}">

  @yield('css')
  
  
  
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Header -->
      @include('layouts.partials.header')
  <!-- Header -->

  <!-- Sidebar -->
      @include('layouts.partials.sidebar')
  <!-- Sidebar -->

  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- Main content -->
        <section class="content">
                <!-- Your Page Content Here -->
              @yield('content')
        </section><!-- /.content -->
        <!-- </div> --><!-- /.content-wrapper -->
</div>
  <!-- /.content-wrapper -->
@include('layouts.partials.footer')



<!-- jQuery -->
<script src="{{ asset("public/plugins/jquery/jquery.min.js")}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset("public/plugins/jquery-ui/jquery-ui.min.js")}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset("public/plugins/bootstrap/js/bootstrap.bundle.min.js")}}"></script>

<!-- jQuery Knob Chart -->
<script src="{{ asset("public/plugins/jquery-knob/jquery.knob.min.js")}}"></script>
<!-- daterangepicker -->
<script src="{{ asset("public/plugins/moment/moment.min.js")}}"></script>
<script src="{{ asset("public/plugins/daterangepicker/daterangepicker.js")}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset("public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js")}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset("public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset("public/js/adminlte.js")}}"></script>
<!-- DataTables -->
<script src="{{ asset("public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") }}"></script>
<script src="{{ asset("public/plugins/datatables-responsive/js/dataTables.responsive.min.js") }}"></script>
<script src="{{ asset("public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") }}"></script>
<!-- SweetAlert2 -->
<script src="{{ asset("public/plugins/sweetalert2/sweetalert2.min.js")}}"></script>
<script src="{{ asset("public/plugins/jquery-validation/jquery.validate.min.js") }}"></script>
<script src="{{ asset("public/plugins/jquery-validation/additional-methods.min.js")}}"></script>
<!-- Toaster alert -->
<script src="{{ asset("public/plugins/toastr/toastr.min.js")}}"></script>
<script src="{{ asset("public/plugins/select2/js/select2.full.min.js")}}"></script>





@yield('javascript')




</div>
</body>
</html>
