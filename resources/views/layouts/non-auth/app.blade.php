<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Practical') }}</title>

      <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("public/plugins/fontawesome-free/css/all.min.css") }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset("public/plugins/icheck-bootstrap/icheck-bootstrap.min.css") }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("public/css/adminlte.min.css")}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset("public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css") }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset("public/css/custom.css") }}">

    <link rel="stylesheet" href="{{ asset("public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css") }}">
    <link rel="stylesheet" href="{{ asset("public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css") }}">

</head>
<body class="login-page" style="min-height: 617.6px;">

    @yield('content')


    <!-- jQuery -->
    <script src="{{ asset("public/plugins/jquery/jquery.min.js")}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset("public/plugins/jquery-ui/jquery-ui.min.js") }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset("public/plugins/bootstrap/js/bootstrap.bundle.min.js")}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset("public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")}}"></script>
    <!-- AdminLTE App -->

    <script src="{{ asset("public/plugins/jquery-validation/jquery.validate.min.js") }}"></script>
    <script src="{{ asset("public/plugins/jquery-validation/additional-methods.min.js")}}"></script>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script> -->
    <script src="{{ asset("public/js/adminlte.js")}}"></script>

    <script src="{{ asset("public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") }}"></script>
    <script src="{{ asset("public/plugins/datatables-responsive/js/dataTables.responsive.min.js") }}"></script>
    <script src="{{ asset("public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") }}"></script>

    <script src="{{ asset("public/plugins/sweetalert2/sweetalert2.min.js")}}"></script>
    <script src="{{ asset("public/plugins/jquery-validation/jquery.validate.min.js") }}"></script>
    <script src="{{ asset("public/plugins/jquery-validation/additional-methods.min.js")}}"></script>
    @yield('javascript')

</body>
</html>
