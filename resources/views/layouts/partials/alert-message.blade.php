@if($errors->any())
	<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
		@foreach($errors->all() as $error)
			<p>{{$error}}</p>
		@endforeach
	</div>
@endif


@if(!empty(session()->has('error')))
	<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
		<p>{{session('error')}}</p>
	</div>
@endif


@if(!empty(session()->has('success')))
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
		<p>{{session('success')}}</p>
	</div>
@endif
