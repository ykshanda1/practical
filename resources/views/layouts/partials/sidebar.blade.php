<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ route('home')}}" class="brand-link">
    <span class="brand-text text-center font-weight-light">{{ config('app.name', 'Practical') }}</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('public/img/user2-160x160.jpg')}}" class="user-image img-circle" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{Auth::user()->name}}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item" >
          <a href="{{ route('dashboard')}}" class="nav-link @if(strpos(' '.request()->route()->getName(), 'dashboard'))active @endif">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview  @if(Request::route()->getName() == 'customers.index' || Request::route()->getName() == 'customers.create') active menu-open   @endif">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Customer
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"  >
                <a href="{{ route('customers.index')}}" class="nav-link text-sm @if(Request::route()->getName() == 'customers.index') active   @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Index</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{ route('customers.create')}}" class="nav-link text-sm @if(Request::route()->getName() == 'customers.create') active   @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </a>
              </li>

            </ul>
        </li>

        <li class="nav-item has-treeview  @if(Request::route()->getName() == 'invoices.index' || Request::route()->getName() == 'invoices.create') active menu-open   @endif">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-circle"></i>
              <p>
                Invoice
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"  >
                <a href="{{ route('invoices.index')}}" class="nav-link text-sm @if(Request::route()->getName() == 'invoices.index') active   @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Index</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{ route('invoices.create')}}" class="nav-link text-sm @if(Request::route()->getName() == 'invoices.create') active   @endif ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Create</p>
                </a>
              </li>

            </ul>
        </li>

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
