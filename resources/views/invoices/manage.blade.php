@extends('layouts.auth.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Invoices</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('invoices.index')}}">Invoices</a></li>
              <li class="breadcrumb-item active">{{empty($invoice)?'Add':'Edit'}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @include('layouts.partials.alert-message')
      <div class="row">
        <div class="col-md-12">
          <div class="card {{ empty($invoice) ? 'card-success' : 'card-primary'}}">
            <div class="card-header">
              <h3 class="card-title">Invoices {{empty($invoice)?'Add':'Edit'}}</h3>
            </div>
            <div class="card-body">
            	<form action="{{ empty($invoice) ? route('invoices.store') : route('invoices.update',[$invoice->id]) }}"method="post" id="manageInvoice">
                  @csrf

                  @empty($invoice)
                    @method('POST')
                  @else
                    @method('PUT')
                    <input type="hidden" id="invoice" value="{{$invoice->id}}">
                  @endif  

                  <div class="form-group">
  	                <label for="customer_id" class="col-md-2 control-label">Customer Name</label>
  	                <div class="col-md-10"> 
                      <select name="customer_id" class="form-control select2 select2-purple" data-dropdown-css-class="select2-purple"  data-placeholder="Select a Customer" style="width: 100%;">
                        @foreach($customers as $customer)
                          <option value="{{$customer->id}}" {{ (!empty($invoice->customers) && $invoice->customer_id == $customer->id) ? 'selected' : '' }}>{{$customer->name}}</option>
                        @endforeach
                      </select>
                      @error('name')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>  
                      @enderror
  	                </div>
  	              </div>
                  <div class="form-group">
                    <label for="date" class="col-md-2 control-label">Due Date</label>
                    <div class="col-md-10">
                      <div class="input-group date" id="due_date_picker" data-target-input="nearest">
                        <input type="text" name="due_date" id="due_date" placeholder="Enter invoice date"  value="{{ !empty($invoice)?$invoice->due_date:'' }}" class="form-control datetimepicker-input" data-target="#due_date_picker" >
                        <div class="input-group-append" data-target="#due_date_picker" data-toggle="datetimepicker">
                            <div class="input-group-text">
                              <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                      </div>
                      @error('due_date')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>  
                      @enderror
                    </div>
                  </div>


                  <h1>Add Invoice Items:- 
                    
                  </h1>
                  

                  <div class="row">
                    <div class="col-md-12">
                      <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th>Product name</th>
                              <th>No. of items</th>
                              <th>Invoice date</th>
                              <th>Amount</th>
                              <th>Sub amount</th>
                              <th><a class="btn btn-info addRow btn-xs float-right" href="#">
                                    <i class="fas fa-plus"></i>
                                    Add more
                                  </a>
                              </th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <input type="text" name="product_name[]" class="form-control @error('product_name') is-invalid @enderror" id="product_name" placeholder="Enter  product name" value="{{ !empty($invoice)?$invoice->product_name:'' }}">
                            </td>
                            <td>
                              <input type="text" name="no_of_items[]" class="form-control @error('no_of_items') is-invalid @enderror" id="no_of_items" placeholder="Enter no of items" value="{{ !empty($invoice)?$invoice->no_of_items:'' }}">
                            </td>
                            <td>
                                <div class="input-group date" id="invoice_date_picker" data-target-input="nearest">
                                  <input type="text" name="invoice_date[]" id="invoice_date" placeholder="Enter invoice date"  value="{{ !empty($invoice)?$invoice->invoice_date:'' }}" class="form-control datetimepicker-input" data-target="#invoice_date_picker" >
                                  <div class="input-group-append" data-target="#invoice_date_picker" data-toggle="datetimepicker">
                                      <div class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                  </div>
                                </div>
                            </td>
                            <td>
                              <input type="text" name="amount[]" class="form-control @error('amount') is-invalid @enderror" id="amount" placeholder="Enter amount">
                            </td>
                            <td>
                              <input type="text" name="subAmount[]" class="form-control" id="amount">
                            </td>
                            <td>
                              <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                          </tr>
                        </tbody>
                      </table>

                      <div class="form-group">
                          <label for="amount" class="col-md-2 control-label">Total Amount</label>
                          <div class="col-md-5">
                            <input type="text" name="amount[]" class="form-control @error('amount') is-invalid @enderror" id="amount" placeholder="Total amount" value="">
                            @error('amount')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>  
                            @enderror
                          </div>
                      </div>
                    </div>
                  </div>

                  <br>
                  <br>
  	              <div class="form-group">
  	                <div class="col-md-offset-2 col-md-10">
  	                  <button type="submit" class="btn btn-primary">Save</button>
  	                  <a href="{{ route('invoices.index') }}" class="btn btn-danger">Cancel</a>
  	                </div>
  	              </div>
	            </form>
	         </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  <!-- /.content-wrapper -->
@endsection


@section('javascript')
  <script src="{{ asset("public/plugins/jquery-validation/jquery.validate.min.js") }}"></script>
  <script src="{{ asset("public/plugins/jquery-validation/additional-methods.min.js")}}"></script>
  <script src="{{ asset("public/plugins/select2/js/select2.full.min.js")}}"></script>
  <script src="{{ asset("public/js/invoices/invoice.js") }}"></script>
  <script>
    $(document).ready(function(){
        Invoice.validateForm();
        $('#due_date_picker').datetimepicker({
            format: 'YYYY-MM-DD',
            
        });

        $('#invoice_date_picker').datetimepicker({
            format: 'YYYY-MM-DD',
            
        });

        $('.select2').select2({
          theme: 'bootstrap4'
        });

    });

    $('.addRow').on('click',function(){
      addRow();
    });

    function addRow(){
      var tr = 
          '<tr>'+
            '<td>'+
              '<input type="text" name="product_name[]" class="form-control @error("product_name") is-invalid @enderror" id="product_name" placeholder="Enter  product name" value="{{ !empty($invoice)?$invoice->product_name:'' }}">'+
            '</td>'+
            '<td>'+
              '<input type="text" name="no_of_items[]" class="form-control @error("no_of_items") is-invalid @enderror" id="no_of_items" placeholder="Enter no of items" value="{{ !empty($invoice)?$invoice->no_of_items:'' }}">'+
            '</td>'+
            '<td>'+
              '<div class="input-group date" id="invoice_date_picker" data-target-input="nearest">'+
                '<input type="text" name="invoice_date[]" id="invoice_date" placeholder="Enter invoice date"  value="{{ !empty($invoice)?$invoice->invoice_date:'' }}" class="form-control datetimepicker-input" data-target="#invoice_date_picker" >'+
                '<div class="input-group-append" data-target="#invoice_date_picker" data-toggle="datetimepicker">'+
                    '<div class="input-group-text">'+
                      '<i class="fa fa-calendar"></i>'+
                    '</div>'+
                '</div>'+
              '</div>'+
            '</td>'+
            '<td>'+
              '<input type="text" name="amount[]" class="form-control @error("amount") is-invalid @enderror" id="amount" placeholder="Enter invoice amount" value="{{ !empty($invoice)?$invoice->amount:'' }}">'+
            '</td>'+

            '<td>'+
              '<input type="text" name="subAmount[]" class="form-control" id="amount">'+  
            '</td>'+

            '<td>'+
              '<a href="#" class="btn btn-danger remove"><i class="fa fa-trash"></i></a>'+
            '</td>'+

          '</tr>';

          $('tbody').append(tr);
    };

    $('tbody').on('click','.remove',function(){
        $(this).parent().parent().remove();
    });

    $('.tbody').each(function(){
      var quantity = parseInt( $(this).find('[name="no_of_items[]"]').val(),10)
      var price = parseInt( $(this).find('[name="amount[]"]').val(),10)
      subTotal += quantity * price;
    });
  </script>

@endsection
