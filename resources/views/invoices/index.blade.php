@extends('layouts.auth.app')

@section('content')
<!-- Site wrapper -->
<div class="wrapper">
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoices</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('invoices.index')}}">Invoices</a></li>
              <li class="breadcrumb-item active">Index</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       @include('layouts.partials.alert-message')
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Invoice Table </h3>
          <div class="card-tools">
            <a href="{{route('invoices.create')}}">
                <button class="btn btn-success text-white">
                    <i class="fas fa-plus"></i>&nbsp;Add New
                </button>
            </a>  
          </div>
        </div>
        <div class="card-body">
            <table id="invoicesTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width: 12%" class="text-center">Customer Name</th>
                        <th style="width: 10%" class="text-center">Customer Email</th>
                        <th style="width: 12%" class="text-center">Invoice Date</th>
                        <th style="width: 12%" class="text-center">Due Date</th>
                        <th style="width: 12%" class="text-center">Total amount</th>
                        <th style="width: 12%" class="text-center">No of Items</th>
                        <th style="width: 12%" class="text-center">Created Date</th>
                        <th style="width: 12%" class="text-center">Action</th>


                    </tr>
                </thead>
                <tbody>
                  @if(!empty($invoices))
                    @foreach($invoices as $invoice)
                    
                      <tr>
                          <td>{{$invoice->customers->name}}</td>
                          <td>{{$invoice->customers->email}}</td>
                          <td>{{\Carbon\Carbon::parse($invoice->invoiceItems->invoice_date)->format('d/m/y')}}</td>
                          <td>{{\Carbon\Carbon::parse($invoice->due_date)->format('d/m/y')}}</td>
                          <td>{{$invoice->invoiceItems->sum('amount')}}</td>
                          <td>{{$invoice->invoiceItems->no_of_items}}</td>
                          <td>{{\Carbon\Carbon::parse($invoice->created_at)->format('d/m/y')}}</td>
                          <td class="project-actions text-center">
                            <a class="btn btn-primary btn-close btn-xs" href="{{ route('invoices.show',[$invoice->id])}}">
                                <i class="fas fa-eye"></i>
                                View
                            </a>
                        
                            <a class="btn btn-info btn-close btn-xs" href="{{ route('invoices.edit',[$invoice->id])}}">
                                <i class="fas fa-pencil-alt"></i>
                                Edit
                            </a>
 
                            <a href="{{ route('invoices.destroy',[$invoice->id])}}"  class="btn btn-danger btn-close btn-xs delete-btn" style="color: white;" type="submit" >
                                <i class="fas fa-trash"></i>
                                Delete
                            </a>
                          </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
            </table>
          </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
</div>  
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
  <script src="{{ asset("public/plugins/sweetalert2/sweetalert2.min.js")}}"></script>
  <script src="{{ asset("public/js/invoices/invoice.js")}}"></script>
  <script>
    $(document).ready(function(){
        Invoice.deleteInvoice();
        Invoice.list();

    });

    RedirectTo();
  </script>
@endsection
