@extends('layouts.auth.app')
@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Single Invoice</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('invoices.index')}}">Invoice</a></li>
            <li class="breadcrumb-item active">Single Invoice</li>
          </ol>
        </div>
      </div>
    </div>
  </section>


  <section class="content"> 
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Invoice</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong>Customer Name</strong>
              <p class="text-muted">
                {{ $invoice->customers->name }}
              </p>
              <hr>

              <strong>Customer Email</strong>
              <p class="text-muted">
                {{ $invoice->customers->email }}
              </p>
              <hr>

              <strong>Product Name</strong>
              <p class="text-muted">
                {{ $invoice->invoiceItems->product_name }}
              </p>
              <hr>

              <strong>Number of items</strong>
              <p class="text-muted">
                {{ $invoice->invoiceItems->no_of_items }}
              </p>
              <hr>

              <strong>Invoice date</strong>
              <p class="text-muted">
                {{ $invoice->invoiceItems->invoice_date }}
              </p>
              <hr>

              <strong>Total amount</strong>
              <p class="text-muted">
                {{ $invoice->invoiceItems->amount }}
              </p>
              <hr>

              <br>
              <div class="row">
                <div class="col-md-offset-2  col-md-10">
                  <a href="{{ route('invoices.index') }}" class="btn btn-danger">Back</a>
                </div>
              </div>
            </div>
              <!-- /.card-body -->
          </div>
        </div>
      </div> 
    </div>
  </section>

@endsection 

