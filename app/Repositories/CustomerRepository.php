<?php

namespace App\Repositories;

use App\Models\Customers;
use App\User;
use App\Interfaces\CustomerRepositoryInterface;
use Illuminate\Support\Facades\DB;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{

    /**
     * PostController constructor.
     *
     * @param PostRepositoryInterface $post
     */
    public function __construct()
    {
        $this->model = new Customers();

    }

    

}    