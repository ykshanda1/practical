<?php

namespace App\Repositories;

use App\Models\Invoices;
use App\Models\InvoiceItems;
use App\User;
use App\Interfaces\InvoiceRepositoryInterface;
use Illuminate\Support\Facades\DB;

class InvoiceRepository extends BaseRepository implements InvoiceRepositoryInterface
{


	protected $invoiceItems;

    /**
     * PostController constructor.
     *
     * @param PostRepositoryInterface $post
     */
    public function __construct()
    {
        $this->model = new Invoices();

    }

    public function store(array $data)
    {
        try{

            DB::beginTransaction();
            $invoiceData = $data['invoice'];
     
            $invoice = $this->model->create($invoiceData);
            $invoiceItemsData = $data['invoice_items'];
            $invoiceItemsData['invoice_id'] = $invoice->id;
            $invoiceItems = $this->invoiceItems->create($invoiceItemsData);
                     
            DB::commit();
            return $invoice;  
        }catch(Exception $exception){
            DB::rollBack();
            Log::error($exception->getMessage());
            return false;
        }
    }

    public function update($id, array $data)
    {
        try{

            DB::beginTransaction();
            $invoiceData = $data['invoice'];
     
            $invoiceResult = $this->model->findOrFail($id);
            $invoice = $invoiceResult->update($invoiceData->toArray());
            $invoiceItemsData = $data['invoice_items'];
            $invoiceItems = $invoiceItemsData->update(['invoice_id'=>$id],$invoiceItemsData->toArray());
                     
            DB::commit();
            return $invoice;  
        }catch(Exception $exception){
            DB::rollBack();
            Log::error($exception->getMessage());
            return false;
        }
    }

}    