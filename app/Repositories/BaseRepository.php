<?php


namespace App\Repositories;

use App\Interfaces\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{
    

    protected $model;

    public function getAll($relationships=[])
    {
        try{
            return $this->model->with($relationships)->latest()->get();
        }catch(Exception $exception){
            Log::error($exception->getMessage());
            return false;
        }
    }

    public function getById($id,$relationships=[])
    {
        try{
            return $this->model->with($relationships)->findOrFail($id);
        }catch(Exception $exception){
            Log::error($exception->getMessage());
            return false;
        }
    }

    
    public function store(array $data)
    {
        try{
            return $this->model->create($data);
        }catch(Exception $exception){
            Log::error($exception->getMessage());
            return false;
        }
    }

    
    public function update($id, array $data)
    {
        try{
            $result =  $this->model->findOrFail($id);
            $status = $result->update($data);
            if ($status) {
                return $result;
            }
        }catch(Exception $exception){
            Log::error($exception->getMessage());
            return false;
        }
    }


    public function delete($id)
    {
        try{
            $result =  $this->model->findOrFail($id);
            $status = $result->delete();
            if ($status) {
                return $result;
            }
        }catch(Exception $exception){
            Log::error($exception->getMessage());
            return false;
        }
    }

}