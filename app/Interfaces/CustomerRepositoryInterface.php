<?php


namespace App\Interfaces;


interface CustomerRepositoryInterface extends BaseRepositoryInterface
{
    
    public function update($id, array $data);


}