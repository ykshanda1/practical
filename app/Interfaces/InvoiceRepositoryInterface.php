<?php


namespace App\Interfaces;


interface InvoiceRepositoryInterface extends BaseRepositoryInterface
{
    
    public function update($id, array $data);


}