<?php


namespace App\Interfaces;

interface BaseRepositoryInterface
{

    public function getAll($relationships=[]);


    public function getById($id,$relationships=[]);


    public function store(array $data);

   
    public function update($id, array $data);


    public function delete($id);



}