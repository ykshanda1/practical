<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Customers extends Model
{
    protected $table = 'customers';
    use SoftDeletes;

    protected $fillable = [
        'name','email'
    ];

    public function invoices(){
    	$this->hasMany(Invoices::class);
    }
}
