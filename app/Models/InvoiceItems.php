<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class InvoiceItems extends Model
{
    protected $table = 'invoice_items';
    use SoftDeletes;

    protected $fillable = [
        'invoice_id', 'invoice_date','amount','no_of_items'
    ];

    public function invoiceDetail(){
    	$this->belongsTo(Invoices::class);
    }
}
