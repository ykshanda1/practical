<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Invoices extends Model
{
    protected $table = 'invoices';
    use SoftDeletes;

    protected $fillable = [
        'customer_id', 'due_date'
    ];

    public function customers(){
    	$this->belongsTo(Customers::class);
    }

    public function invoiceItems(){
    	$this->hasMany(InvoiceItems::class);
    }
}
