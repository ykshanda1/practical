<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|string|max:250',
            'due_date' => 'required|date',
            'product_name' => 'required|string|max:250',
            'invoice_date' => 'required|date',
            'no_of_items' => 'required|numeric',
            'amount' => 'required|numeric',
        ];
    }
}
