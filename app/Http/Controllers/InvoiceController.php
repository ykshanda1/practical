<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Requests\InvoiceRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Validator;
use Carbon\Carbon;
use File;
use App\User;
use App\Models\Invoices;
use App\Models\Customers;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Interfaces\InvoiceRepositoryInterface;
use App\Repositories\InvoiceRepository;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $invoiceRepository;

    public function __construct(InvoiceRepositoryInterface $invoiceRepository){
        $this->invoiceRepository = $invoiceRepository;

    }


    public function index()
    {
        $invoices = $this->invoiceRepository->getAll(['customers','invoiceDetail','invoiceItems']);
        return view('invoices.index',['invoices'=>$invoices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customers::all();
        return view('invoices.manage',['customers'=>$customers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        try{

            $inputInvoice['customer_id'] = $request->customer_id;
            $inputInvoice['due_date'] = $request->due_date;

            $inputInvoiceItems['invoice_date'] = $request->invoice_date;
            $inputInvoiceItems['product_name'] = $request->product_name;
            $inputInvoiceItems['no_of_items'] = $request->no_of_items;
            $inputInvoiceItems['amount'] = $request->amount;

            //dd($inputInvoice,$inputInvoiceItems);
            $data = [
                'invoice' => $inputInvoice,
                'invoice_items' => $inputInvoiceItems
            ];

            $customer = $this->invoiceRepository->store($inputData);

            if(!empty($customer)){
                return redirect()->route('invoices.index')->with('success', 'Invoices added successfully!!');
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{

            $invoice = $this->invoiceRepository->getById($id,['customers','invoiceDetail','invoiceItems']);
            if(!empty($invoice)){
                return view('invoices.show',compact('invoice'));
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{

            $invoice = $this->invoiceRepository->getById($id,['customers','invoiceDetail','invoiceItems']);
            if(!empty($invoice)){
                return view('invoices.manage',compact('invoice'));
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $invoice = $this->invoiceRepository->getById($id,['customers','invoiceDetail','invoiceItems']);
            $invoice->customer_id = $request->customer_id;
            $invoice->due_date = $request->due_date;
            
            $invoice->invoiceItems->invoice_date = $request->invoice_date;
            $invoice->product_name = $request->product_name;
            $invoice->no_of_items = $request->no_of_items;
            $invoice->amount = $request->amount;

            $data = [
                'invoice' => $invoice,
                'invoice_items' => $invoice->invoiceItems
            ];

            $invoice = $this->invoiceRepository->update($id,$data);

            if(!empty($invoice)){
                return redirect()->route('invoices.index')->with('success', 'Invoices added successfully!!');
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $invoice = $this->invoiceRepository->delete($id);
            if(!empty($invoice)){
                return  response()->json(['status'=> 'success']);
            }
        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
        return  response()->json(['status'=> 'error']); 
    }
}
