<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Validator;
use Carbon\Carbon;
use File;
use App\User;
use App\Models\Customers;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Interfaces\CustomerRepositoryInterface;
use App\Repositories\CustomerRepository;
use Illuminate\Support\Facades\DB;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $customerRepository;

    public function __construct(CustomerRepositoryInterface $customerRepository){
        $this->customerRepository = $customerRepository;

    }

    public function index()
    {
        $customers = $this->customerRepository->getAll();
        return view('customers.index',['customers'=>$customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        try{

            $inputData['name'] = $request->name;
            $inputData['email'] = $request->email;

            $customer = $this->customerRepository->store($inputData);

            if(!empty($customer)){
                return redirect()->route('customers.index')->with('success', 'Customers added successfully!!');
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{

            $customer = $this->customerRepository->getById($id);
            if(!empty($customer)){
                return view('customers.show',compact('customer'));
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{

            $customer = $this->customerRepository->getById($id);
            if(!empty($customer)){
                return view('customers.manage',compact('customer'));
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            $customer = $this->customerRepository->getById($id);
            $customer->name = $request->name;
            $customer->email = $request->email;

            $customerResult = $customer->update($customer->toArray());

            if(!empty($customerResult)){
                return redirect()->route('customers.index')->with('success', 'Customers updated successfully!!');
            }

        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
        return redirect()->route('customers.index')->withErrors('Customers is failed to updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $customer = $this->customerRepository->delete($id);
            if(!empty($customer)){
                return  response()->json(['status'=> 'success']);
            }
        }catch(Exception $exception){
            Log::error($exception->getMessage());
        }
        return  response()->json(['status'=> 'error']); 
    }
}
